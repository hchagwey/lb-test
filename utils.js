const MQU = require("../measure-query-gen/index");

async function initMQG() {
  const MQG = await MQU.init("http://localhost:8080/OLAP");
  global.MQG = MQG;
}

module.exports = initMQG;
