async function getMeasure(measure) {
  const data = await MQG.QueryView().measure(measure).get();
  return parseInt(data[0][measure]);
}

module.exports = { getMeasure };
