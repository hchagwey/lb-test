const initMQG = require("./utils");
const { getMeasure } = require("./helpers");

beforeAll(() => {
  return initMQG();
});

test("NetSales = Sales - Returns", async () => {
  const netSales = await getMeasure("NetSales");
  const sales = await getMeasure("Sales");
  const returns = await getMeasure("Returns");
  expect(netSales).toBe(sales - returns);
});
